

//Subject Credits
const js = 4;
const react = 7;
const python = 6;
const java = 3;
const allCredit = js + react + python + java;

// First student array;
let student1=[];
student1["name"] = "Jean";
student1["lastName"] = "Reno";
student1["age"] =   26;
student1["subject"] = [];
student1["subject"]["javascript"] = 62;
student1["subject"]["react"] =  57;
student1["subject"]["python"] = 88; 
student1["subject"]["java"] = 90;
student1["statistic"] = [];
student1["statistic"]["sumOfMarks"] = student1["subject"]["javascript"] + student1["subject"]["react"] + student1["subject"]["python"] + student1["subject"]["java"];
student1["statistic"]["averageMark"] = student1["statistic"]["sumOfMarks"] / 4;  // student1["subject"].length; ??
student1["statistic"]["GPA"] =   (js + 0.5 * react + 3 * python + 3 * java) / allCredit; 

// Second student array;
let student2=[];
student2["name"] = "Klod";
student2["lastName"] = "Mone";
student2["age"] = 19;             
student2["subject"]  = [];
student2["subject"]["javascript"] = 77;
student2["subject"]["react"] =  52;
student2["subject"]["python"] = 92; 
student2["subject"]["java"] = 67;
student2["statistic"] = [];
student2["statistic"]["sumOfMarks"] = student2["subject"]["javascript"] + student2["subject"]["react"] + student2["subject"]["python"] + student2["subject"]["java"];
student2["statistic"]["averageMark"] = student2["statistic"]["sumOfMarks"] / 4;  // student2["subject"].length; ??
student2["statistic"]["GPA"]  =  (2 * js + 0.5 * react + 4 * python + java) / allCredit; 


// Third student array;
let student3=[];
student3["name"] = "Van";
student3["lastName"] = "Gogh";
student3["age"] = 21;             
student3["subject"]  = [];
student3["subject"]["javascript"] = 51;
student3["subject"]["react"] =  98;
student3["subject"]["python"] = 65; 
student3["subject"]["java"] = 70;
student3["statistic"] = [];
student3["statistic"]["sumOfMarks"] = student3["subject"]["javascript"] + student3["subject"]["react"] + student3["subject"]["python"] + student3["subject"]["java"];
student3["statistic"]["averageMark"] = student3["statistic"]["sumOfMarks"] / 4;  // student3["subject"].length; ??
student3["statistic"]["GPA"]  = (0.5 * js + 4 * react + python + java) / allCredit; 

// Fourth student array;
let student4=[];
student4["name"] = "Dam";
student4["lastName"] = "Square";
student4["age"] = 36;             
student4["subject"]  = [];
student4["subject"]["javascript"] = 82;  
student4["subject"]["react"] =  53;
student4["subject"]["python"] = 80; 
student4["subject"]["java"] = 65;
student4["statistic"] = [];
student4["statistic"]["sumOfMarks"] = student4["subject"]["javascript"] + student4["subject"]["react"] + student4["subject"]["python"] + student4["subject"]["java"];
student4["statistic"]["averageMark"] = student4["statistic"]["sumOfMarks"] / 4;  //student4["subject"].length;
student4["statistic"]["GPA"]  =  (3 * js + 0.5 * react + 2 * python + java) / allCredit; 

// Average mark of all students;
let averageMarkOfAllStudents = ( student1["statistic"]["sumOfMarks"] + student2["statistic"]["sumOfMarks"] + student3["statistic"]["sumOfMarks"] + student4["statistic"]["sumOfMarks"] ) / 16;

student1["Degree"] = student1["statistic"]["averageMark"] > averageMarkOfAllStudents ? "Red Diploma" : "Enemy of People";
student2["Degree"] = student2["statistic"]["averageMark"] > averageMarkOfAllStudents ? "Red Diploma" : "Enemy of People";
student3["Degree"] = student3["statistic"]["averageMark"] > averageMarkOfAllStudents ? "Red Diploma" : "Enemy of People";
student4["Degree"] = student4["statistic"]["averageMark"] > averageMarkOfAllStudents ? "Red Diploma" : "Enemy of People";

// The best student with GPA  
if(student1["statistic"]["GPA"] > student2["statistic"]["GPA"] && student1["statistic"]["GPA"] > student3["statistic"]["GPA"] && student1["statistic"]["GPA"] > student4["statistic"]["GPA"]){
    console.log(`The best student is ${student1["name"]} ${student1["lastName"]} with his GPA --> ${student1["statistic"]["GPA"]};`);
} else if(student2["statistic"]["GPA"] > student3["statistic"]["GPA"]  &&  student2["statistic"]["GPA"] > student4["statistic"]["GPA"]){
    console.log(`The best student is ${student2["name"]} ${student2["lastName"]} with his GPA --> ${student2["statistic"]["GPA"]};`);
} else if (student3["statistic"]["GPA"] > student4["statistic"]["GPA"]){
    console.log(`The best student is ${student3["name"]} ${student3["lastName"]} with his GPA --> ${student3["statistic"]["GPA"]};`);
} else {
    console.log(`The best student is ${student4["name"]} ${student4["lastName"]} with his GPA --> ${student4["statistic"]["GPA"]};`);
}

// The best student with averageMark and over 21

if( student1["age"] > 21 && (student1["statistic"]["averageMark"] > student2["statistic"]["averageMark"] && student1["statistic"]["averageMark"] > student3["statistic"]["averageMark"] && student1["statistic"]["averageMark"] > student4["statistic"]["averageMark"])){
   console.log(`The best student is ${student1["name"]} ${student1["lastName"]} over 21 with his average of marks --> ${student1["statistic"]["averageMark"]};`);   
} else if( student2["age"] > 21 && (student2["statistic"]["averageMark"] > student3["statistic"]["averageMark"] && student2["statistic"]["averageMark"] > student4["statistic"]["averageMark"])){
    console.log(`The best student is ${student2["name"]} ${student2["lastName"]} over 21 with his average of marks --> ${student2["statistic"]["averageMark"]};`);   
} else if( student3["age"] > 21 && (student3["statistic"]["averageMark"] > student4["statistic"]["averageMark"])){
    console.log(`The best student is ${student3["name"]} ${student3["lastName"]} over 21 with his average of marks --> ${student3["statistic"]["averageMark"]};`);   
} else if( student4["age"] > 21){
    console.log(`The best student is ${student4["name"]} ${student4["lastName"]} over 21 with his average of marks --> ${student4["statistic"]["averageMark"]};`);   
} else {
    console.log("No one is over 21;");
}

// The best in Front-end subjects
let frontEndMarkStudent1 = (student1["subject"]["javascript"] + student1["subject"]["react"]) / 2;
let frontEndMarkStudent2 = (student2["subject"]["javascript"] + student2["subject"]["react"]) / 2;
let frontEndMarkStudent3 = (student3["subject"]["javascript"] + student3["subject"]["react"]) / 2;
let frontEndMarkStudent4 = (student4["subject"]["javascript"] + student4["subject"]["react"]) / 2;

if(frontEndMarkStudent1 > frontEndMarkStudent2 && frontEndMarkStudent1 > frontEndMarkStudent3 && frontEndMarkStudent1 > frontEndMarkStudent4){
    console.log(`The best is ${student1["name"]} ${student1["lastName"]} in Front-end subjects;`);
} else if(frontEndMarkStudent2 > frontEndMarkStudent3 && frontEndMarkStudent2 > frontEndMarkStudent4){
    console.log(`The best is ${student2["name"]} ${student2["lastName"]} in Front-end subjects;`);
} else if( frontEndMarkStudent3 > frontEndMarkStudent4){
    console.log(`The best is ${student3["name"]} ${student3["lastName"]} in Front-end subjects;`);
} else {
    console.log(`The best is ${student4["name"]} ${student4["lastName"]} in Front-end subjects;`);
}

